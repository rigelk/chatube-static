import { instances } from "./instances.json"

const choice = instances[Math.floor(Math.random() * instances.length)]
window.location.replace(
  `https://${choice}${window.location.pathname}${window.location.search}`
)
