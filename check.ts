import Axios from "axios"
import fs from "fs"
import { instances } from "./instances.json"
const instanceTestPage = "watch?v=DoYXjOk0uys"

const getInstanceStatus = async instance => {
  const requestStatus = await Axios.get(
    `https://${instance.host}/${instanceTestPage}`,
    {
      timeout: 3000,
      headers: { "User-Agent": "CHATube" }
    }
  )
    .then(res => res.status)
    .catch(err => null)
  return requestStatus || 500
}

;(async () => {
  let checkedInstances = instances.map(i => {
    return { host: i, status: 200 }
  })
  checkedInstances = await Promise.all(
    checkedInstances.map(async i => {
      return {
        ...i,
        status: await getInstanceStatus(i)
      }
    })
  )
  console.log(checkedInstances)
  const goodInstances = checkedInstances
    .filter(
      i =>
        i.status.toString().startsWith("2") && i.status.toString().length === 3
    )
    .map(i => i.host)
  fs.writeFile("./instances.json", JSON.stringify({ instances: goodInstances }), err => {
    if (err) {
      console.error(err)
      return
    }
    console.log("saved operational instances :", goodInstances)
  })
})()
